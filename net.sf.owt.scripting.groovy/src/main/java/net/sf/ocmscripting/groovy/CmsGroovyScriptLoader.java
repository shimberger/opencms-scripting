/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.groovy;

import java.io.IOException;

import net.sf.ocmscripting.CmsScriptLoader;
import net.sf.ocmscripting.CmsScriptingUtil;
import net.sf.ocmscripting.I_ScriptRequestUpdater;

import org.apache.commons.logging.Log;
import org.opencms.configuration.CmsConfigurationException;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

/**
 * Resource loader for Groovlets.
 * <p>
 */
public class CmsGroovyScriptLoader extends CmsScriptLoader implements
        I_ScriptRequestUpdater {

    /**
     * The logging channel for this class.
     */
    private static Log LOG = CmsLog.getLog(CmsGroovyScriptLoader.class);

    /**
     * The ID of this resource loader
     */
    public static final int LOADER_ID = 9992;

    /**
     * The default repository folder
     */
    public static final String DEFAULT_REPOSITORY = "groovlets";

    /**
     * The suffix for groovlets in THE RFS
     */
    private static final String SCRIPT_SUFFIX = ".groovy";

    /**
     * Returns the suffix for scripts of this type
     */
    public String getScriptSuffix() {
        return SCRIPT_SUFFIX;
    }

    /**
     * Groovlets are always committed.
     * 
     * @return <code>true</code>
     */
    protected boolean isAlwaysCommitted() {
        return true;
    }

    /**
     * @see net.sf.ocmscripting.CmsScriptLoader#getScriptUpdater()
     */
    protected I_ScriptRequestUpdater getScriptUpdater() {
        return this;
    }

    /**
     * Returns the resource loader ID.
     * 
     * @return The ID of this resource loader
     */
    public int getLoaderId() {
        return LOADER_ID;
    }

    /**
     * @see org.opencms.loader.I_CmsResourceLoader#getResourceLoaderInfo()
     */
    public String getResourceLoaderInfo() {
        return Messages.get().getBundle().key(Messages.RESOURCELOADER_INFO_0);
    }

    /**
     * Ensures that included/references files and the actual file is present in
     * the RFS.
     */
    protected void ensureRFSCopies(CmsObject cms, CmsResource resource,
            String rfsPath) throws CmsException, IOException {
        if (LOG.isDebugEnabled()) {
            LOG.debug(Messages.get().getBundle().key(
                    Messages.DEBUG_ENSURE_RFS_COPIES_1, resource));
        }
        if (CmsScriptingUtil.isRFSUpdateNeeded(rfsPath, resource)) {
            CmsFile file = cms.readFile(resource);
            writeFile(rfsPath, resolveLinkMacros(extractSourcecode(file), cms));
        }
    }

    /**
     * Initialized the configuration.
     */
    public void initConfiguration() throws CmsConfigurationException {
        if (LOG.isInfoEnabled()) {
            LOG.info(Messages.get().getBundle().key(
                    Messages.LOADER_INITIALIZING_CONFIG_0));
        }

        // Read the configuration
        String repository = OpenCms.getSystemInfo()
                .getAbsoluteRfsPathRelativeToWebInf(DEFAULT_REPOSITORY);

        // Log the configuration
        if (LOG.isInfoEnabled()) {
            LOG.info(Messages.get().getBundle().key(
                    Messages.LOADER_SET_REPOSITORY_1, repository));
        }

        // Set the configuration
        setRepository(repository);

    }

}
