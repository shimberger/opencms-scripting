/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.groovy;

import groovy.lang.Binding;
import groovy.lang.Closure;
import groovy.servlet.AbstractHttpServlet;
import groovy.servlet.ServletBinding;
import groovy.servlet.ServletCategory;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.codehaus.groovy.runtime.GroovyCategorySupport;
import org.opencms.main.CmsLog;

/**
 * Modified GroovyServlet to better respect OpenCms internals.
 * <p>
 * 
 * @see groovy.servlet.GroovyServlet
 */
public class CmsGroovyServlet extends AbstractHttpServlet {

    /**
     * The logging channel for this class.
     */
    private static Log LOG = CmsLog.getLog(CmsGroovyServlet.class);

    /**
     * The variable name where the PageContext is bound to through the
     * ServletBinding.
     */
    public static final String VAR_PAGECONTEXT = "pageContext";

    /**
     * The default content type for the response.
     */
    public static final String DEFAULT_CONTENT_TYPE = "text/html";

    /**
     * The scripting engine used for execution.
     */
    private GroovyScriptEngine gse;

    /**
     * Initialize the GroovyServlet.
     * 
     * @throws ServletException
     *             If this method encountered difficulties
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        // Set up the scripting engine
        gse = createGroovyScriptEngine();

        if (LOG.isInfoEnabled()) {
            LOG.info(Messages.get().getBundle().key(
                    Messages.GROOVY_SERVLET_INITIALIZED_1, gse.toString()));
        }
    }

    /**
     * Handle web requests to the GroovyServlet
     * 
     * @see groovy.servlet.GroovyServlet#service(HttpServletRequest,
     *      HttpServletResponse)
     */
    public void service(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        // Get the script path from the request - include aware (GROOVY-815)
        final String scriptUri = getScriptUri(request);

        // Set it to HTML by default
        response.setContentType(DEFAULT_CONTENT_TYPE);

        // Set up the script context
        final Binding binding = new ServletBinding(request, response,
                servletContext);

        // Create a JSP PageContext to allow the Groovlets to
        // use OpenCms JSP functionality.
        // TODO: Modify Servlet to read these values from the executed Groovlet
        JspFactory factory = JspFactory.getDefaultFactory();
        PageContext pageContext = factory.getPageContext(this, // The servlet
                request, // The request
                response,// The response
                null, // The errorPageURL
                false, // The needsSession variable
                4096, // The buffer size
                true // The autoFlush variable
                );

        // Bind the pageContext variable to the Groovlet
        binding.setVariable(VAR_PAGECONTEXT, pageContext);

        if (LOG.isDebugEnabled()) {
            LOG.debug(Messages.get().getBundle().key(
                    Messages.GROOVY_SERVLET_EXECUTING_2, scriptUri, binding));
        }

        // Run the script
        try {
            Closure closure = new Closure(gse) {
                public Object call() {
                    try {
                        return ((GroovyScriptEngine) getDelegate()).run(
                                scriptUri, binding);
                    } catch (Exception e) {
                        // We have to wrap the exception because the call()
                        // method does not throw any checked exceptions
                        throw new RuntimeException(e);
                    }
                }
            };
            GroovyCategorySupport.use(ServletCategory.class, closure);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (RuntimeException re) {
            // This block catches all exceptions thrown in the
            // script execution closure.
            doHandleException(scriptUri, re);
            return;
        } finally {

            // We always flush the stream and release
            // the PageContext
            response.getOutputStream().flush();
            if (factory != null) {
                factory.releasePageContext(pageContext);
            }

        }
    }

    /**
     * Handle exceptions thrown by the script closure.
     * <p>
     * 
     * @param script
     *            The name/path of the Groovlet
     * @param re
     *            The RuntimeException where the original ones are wrapped in.
     * @throws ServletException
     *             A ServletException used to indicate the failure to the
     *             container
     */
    protected void doHandleException(String script, RuntimeException re)
            throws ServletException {
        if (LOG.isErrorEnabled()) {
            LOG.error(Messages.get().getBundle().key(
                    Messages.GROOVY_SERVLET_HANDLE_EXCEPTION_1, script));
        }

        Throwable cause = re.getCause();
        if (cause == null) {
            // If there's no cause simply throw a ServletException
            throw new ServletException(re);
        }

        // Buffer to hold a more usable error message
        StringBuffer errorMessage = new StringBuffer();

        // Handle the ResourceException
        if (cause instanceof ResourceException) {
            errorMessage.append(Messages.get().getBundle().key(
                    Messages.GROOVY_SERVLET_RESOURCE_EXCEPTION_1, script));
            errorMessage.append(Messages.get().getBundle()
                    .key(Messages.GROOVY_SERVLET_EXCEPTION_WAS_1,
                            cause.getMessage()));
        }

        // Handle the ScriptException
        if (cause instanceof ScriptException) {
            errorMessage.append(Messages.get().getBundle().key(
                    Messages.GROOVY_SERVLET_SCRIPT_EXCEPTION_1, script));
            errorMessage.append(Messages.get().getBundle()
                    .key(Messages.GROOVY_SERVLET_EXCEPTION_WAS_1,
                            cause.getMessage()));
        }

        // Write the full exception to the log
        if (LOG.isErrorEnabled()) {
            LOG.error(errorMessage, cause);
        }

        // Finally throw a ServletExcption with the real cause
        throw new ServletException(cause);

    }

    /**
     * Hook method to setup the GroovyScriptEngine to use.<br/> Subclasses may
     * override this method to provide a custom engine.
     */
    protected GroovyScriptEngine createGroovyScriptEngine() {
        return new GroovyScriptEngine(this);
    }

}
