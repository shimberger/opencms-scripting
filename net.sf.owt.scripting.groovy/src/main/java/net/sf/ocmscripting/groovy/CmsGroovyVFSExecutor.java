/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.groovy;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;

/**
 * Helper class to allow execution of Groovy scripts from VFS files.
 * <p>
 * 
 */
public class CmsGroovyVFSExecutor {

    /**
     * The logging channel for this class.
     */
    private static Log LOG = CmsLog.getLog(CmsGroovyVFSExecutor.class);

    /**
     * The CmsObject used to read the scripts.
     */
    private CmsObject cms;

    /**
     * Constructs a new instance. The CmsObject is used for reading the script
     * files.
     * 
     * @param cms
     *            The CmsObject used for reading the script files
     */
    public CmsGroovyVFSExecutor(CmsObject cms) {
        if (cms == null) {
            throw new NullPointerException("CmsObject passed into "
                    + this.getClass().getName()
                    + " constructor must not be null.");
        }
        this.cms = cms;
    }

    /**
     * Executes the script denoted by the path with the provided binding.
     * <p>
     * 
     * Beware that the CmsObject supplied in the constructor is used to read the
     * script. So the path must match the request context (e.g. site root) in
     * the supplied CmsObject.
     * 
     * @param path
     *            The VFS path to script
     * @param binding
     *            The binding used for the script
     * @return The return value of the evaluated script
     * @throws CmsException
     *             If an OpenCms operation fails
     * @throws NullPointerException
     *             If a null path is supplied
     */
    public Object executeScript(String path, Binding binding)
            throws CmsException {
        if (path == null) {
            if (LOG.isErrorEnabled()) {
                LOG.error(Messages.get().getBundle().key(
                        Messages.ERROR_VFS_EXECUTOR_SCRIPT_NULL_0));
            }
            throw new NullPointerException(Messages.get().getBundle().key(
                    Messages.ERROR_VFS_EXECUTOR_SCRIPT_NULL_0));
        }

        Object returnValue = null;

        // The binding contains the variables exported to
        // the script.
        GroovyShell shell = new GroovyShell(binding);

        try {

            // Read the script file from the VFS
            CmsFile scriptFile = readScriptResource(path);

            // Evaluate the script source code with the created shell
            returnValue = shell.evaluate(new String(scriptFile.getContents()));

        } catch (CmsException cmse) {
            // Intercept exception to log it
            if (LOG.isErrorEnabled()) {
                LOG.error(Messages.get().getBundle().key(
                        Messages.ERROR_VFS_EXECUTOR_SCRIPT_ERR_0), cmse);
            }
            throw cmse;
        } catch (RuntimeException re) {
            // Intercept exception to log it
            if (LOG.isErrorEnabled()) {
                LOG.error(Messages.get().getBundle().key(
                        Messages.ERROR_VFS_EXECUTOR_SCRIPT_ERR_0), re);
            }
            throw re;
        }

        return returnValue;
    }

    /**
     * Utility method to read the CmsFile from the VFS.
     * <p>
     * 
     * @param path
     *            The path to read the script from
     * @return The read CmsFile
     * @throws CmsException
     *             If some VFS operation fails or the script is not readable
     */
    private CmsFile readScriptResource(String path) throws CmsException {
        CmsResource resource = cms.readResource(path);
        // TODO: Fix deprecated method call CmsFile#upgrade(res,cms)
        CmsFile scriptFile = cms.readFile(resource);
        return scriptFile;
    }

}
