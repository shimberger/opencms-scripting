/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.groovy;

import groovy.lang.Binding;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsLog;
import org.opencms.scheduler.I_CmsScheduledJob;

/**
 * Simple class to execute Groovy scripts as scheduled tasks.
 * <p>
 * 
 * The script executed can be supplied via the "script" parameter. The CmsObject
 * passed the the launch-method is used to read the script.
 * 
 * The {@link CmsScheduledJobBinding} is used to bind the launch parameters to
 * the script.
 */
public class CmsGroovyScriptTask implements I_CmsScheduledJob {

    /**
     * The logging channel for this class.
     */
    private static Log LOG = CmsLog.getLog(CmsGroovyScriptTask.class);

    /**
     * The parameter name to supply the script path
     */
    public static final String PARAM_SCRIPT = "script";

    /**
     * The return value of the executed script will be converted to a string. If
     * there's no return value <code>null</code> will be returned.
     * 
     * @see I_CmsScheduledJob#launch(CmsObject, Map)
     */
    public String launch(CmsObject cms, Map parameters) throws Exception {

        if (LOG.isDebugEnabled()) {
            LOG.debug(Messages.get().getBundle().key(
                    Messages.DEBUG_LAUNCH_SCRIPT_TASK_0));
        }

        // Read the script path and throw exception if it does not exist
        String scriptPath = (String) parameters.get(PARAM_SCRIPT);
        if (scriptPath == null || !cms.existsResource(scriptPath)) {
            throw new Exception(Messages.get().getBundle().key(
                    Messages.JOB_SCRIPT_NOT_FOUND_1, scriptPath));
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(Messages.get().getBundle().key(
                    Messages.DEBUG_EXECUTING_TASK_SCRIPT_2, scriptPath,
                    cms.toString()));
            for (Iterator i = parameters.keySet().iterator(); i.hasNext();) {
                Object key = i.next();
                Object value = parameters.get(key);
                LOG.debug(Messages.get().getBundle().key(
                        Messages.DEBUG_EXECUTING_TASK_SCRIPT_PARAM_2,
                        key.toString(), value.toString()));
            }
        }

        // Create the binding and execute the script with it
        Binding binding = new CmsScheduledJobBinding(cms, parameters);
        CmsGroovyVFSExecutor executor = new CmsGroovyVFSExecutor(cms);
        Object returnValue = executor.executeScript(scriptPath, binding);

        // Handle eventual return values
        if (returnValue != null) {
            return returnValue.toString();
        }

        // By default we always return null
        return null;
    }

}
