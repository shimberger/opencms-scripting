/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.groovy;

import groovy.lang.Binding;

import java.util.Map;

import org.opencms.file.CmsObject;

/**
 * Simple binding to export the scheduled job parameters to a Groovy script.
 * <p>
 */
public class CmsScheduledJobBinding extends Binding {

    /**
     * Variable name for the CmsObject
     */
    public static final String VAR_CMS = "cms";

    /**
     * Variable name for the parameter Map
     */
    public static final String VAR_PARAMETERS = "params";

    /**
     * Constructs a new Binding using the given objects.
     * <p>
     * 
     * If one of the references is <code>null</code> throws a
     * NullPointerException.
     * 
     * @param cms
     *            The CmsObject for this binding
     * @param parameters
     *            The parameter MAP
     * @throws NullPointerException
     *             If one of the parameters is <code>null</code>.
     */
    public CmsScheduledJobBinding(CmsObject cms, Map parameters) {
        super();
        if (cms == null || parameters == null) {
            StringBuffer sb = new StringBuffer();
            // TODO: Use Messages for errors in scheduled job binding
            sb
                    .append("Can't constructs a CmsScheduledJobBinding if either the CmsObject or the parameter Map is null.");
            if (cms == null) {
                sb.append(" The CmsObject is null.");
            }
            if (parameters == null) {
                sb.append(" The parameter Map is null.");
            }
            throw new NullPointerException(sb.toString());
        }
        setVariable(VAR_CMS, cms);
        setVariable(VAR_PARAMETERS, parameters);
    }

}
