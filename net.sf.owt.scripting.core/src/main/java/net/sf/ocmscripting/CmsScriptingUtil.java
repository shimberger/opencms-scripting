/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting;

import java.io.File;

import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.flex.CmsFlexController;
import org.opencms.main.CmsException;
import org.opencms.main.OpenCms;
import org.opencms.util.CmsFileUtil;

/**
 * Utility class for scripting integrations.
 * <p>
 */
public class CmsScriptingUtil {

    /**
     * Path delimiter inside the OpenCms VFS.
     */
    private static final String PATH_DELIMITER = "/";

    /**
     * Computes the RFS path of a resource.
     * <p>
     * 
     * @see #computeRFSPath(String, CmsObject, String)
     * 
     * @param repository
     *            The repository directory
     * @param cms
     *            The CmsObject used for the request
     * @param resource
     *            The CmsResource to compute the RFS path for
     * @return The absolute RFS path for this resource
     */
    public static String computeRFSPath(final String repository,
            final CmsObject cms, final CmsResource resource) {
        if (resource == null) {
            throw new NullPointerException(
                    "Can't compute RFS path using a null pointer as resource.");
        }
        return computeRFSPath(repository, cms, resource.getRootPath());
    }

    /**
     * Computes the RFS path of a resource.
     * <p>
     * 
     * This method computes the repository path inside the RFS for a resource.
     * It takes into account the current project (online / offline).
     * 
     * @param repository
     *            The path to the repository
     * @param cms
     *            The CmsObject used for the request
     * @param rootPath
     *            The root path of the Resource
     * @return The absolute path for the resource
     */
    public static String computeRFSPath(final String repository,
            final CmsObject cms, final String rootPath) {
        if (repository == null || cms == null || rootPath == null) {
            throw new NullPointerException(
                    "Can't compute RFS path using null pointers [repository: "
                            + repository + ",cms: " + cms + "rootPath: "
                            + rootPath + "].");
        }
        String repositoryPath = CmsFileUtil.getRepositoryName(repository
                + File.separatorChar, rootPath, cms.getRequestContext()
                .currentProject().isOnlineProject());
        return repositoryPath;
    }

    /**
     * Ensures that the resource specified ends with the provided suffix.
     * <p>
     * 
     * @param path
     *            The complete path to the resource
     * @param suffix
     *            The suffix (include the "." (dot)) e.g. ".php"
     * @return The resource path with appended suffix if necessary
     */
    public static String ensureSuffix(String path, String suffix) {
        if (path == null || suffix == null) {
            throw new NullPointerException(
                    "Can't ensure suffix by using path '" + path
                            + "' and suffix '" + suffix + "'");
        }
        if (!path.endsWith(suffix)) {
            return path + suffix;
        }
        return path;
    }

    /**
     * Removes the webapplication rfs prefix from a path.
     * <p>
     * 
     * This method removes the path of the webapplication from a RFS path. It
     * modifies the provided path only if needed.
     * 
     * @param path
     *            The path
     * @return The path without the prefix
     */
    public static String removeWebappPrefix(final String path) {
        if (path == null) {
            throw new NullPointerException(
                    "Can't remove prefix from null path.");
        }
        String webappRfsPath = OpenCms.getSystemInfo()
                .getWebApplicationRfsPath();
        if (path.startsWith(webappRfsPath)) {
            return path.substring(webappRfsPath.length() - 1);
        }
        return path;
    }

    /**
     * Checks if the provided OpenCms path is absolute.
     * <p>
     * 
     * @param path
     *            The OpenCms path
     * @return <code>true</code> if the path is absolute
     */
    public static boolean isAbsolutePath(final String path) {
        if (path == null) {
            throw new NullPointerException(
                    "Can't check for absolute path if path is null.");
        }
        return path.startsWith(PATH_DELIMITER);
    }

    /**
     * Checks if a RFS update is needed for the given resource.
     * <p>
     * 
     * The method compares if the RFS file exists or is outdated and returns if
     * an update (or initial export) is required.
     * 
     * @param rfsPath
     *            The RFS path representing the resource
     * @param resource
     *            The CmsResource
     * @return <code>true</code> If an export/update is needed
     */
    public static boolean isRFSUpdateNeeded(String rfsPath, CmsResource resource) {
        File rfsFile = new File(rfsPath);
        if (!rfsFile.exists()
                || rfsFile.lastModified() < resource.getDateLastModified()) {
            // if file neither does exist nor is up to date
            return true;
        }
        return false;
    }

    /**
     * Reads a CmsResource representing a script using the root path.
     * 
     * @see org.opencms.file.CmsObject#readResource(String)
     * 
     * @param controller
     *            The currently used CmsFlexController
     * @param path
     *            The complete path to the resource (including sites prefix)
     * @return The read resource
     * @throws CmsException
     *             If something goes wrong or the resource could no be found
     */
    // This method is a nearly 1:1 copy of the JSP integration
    public static CmsResource readScriptResource(CmsFlexController controller,
            String path) throws CmsException {

        // create an OpenCms user context that operates in the root site
        CmsObject cms = OpenCms.initCmsObject(controller.getCmsObject());
        // we only need to change the site, but not the project,
        // since the request has already the right project set
        cms.getRequestContext().setSiteRoot("");
        // try to read the resource
        return cms.readResource(path);
    }
    
    /**
     * Ensures that all Windows like Path delimiters are 
     * transformed to their VFS counterpart.
     * 
     * @param path The path to transform
     * @return The transformed path
     */
    public static String ensureVfsDelimiters(String path) {
    	return path.replace('\\', '/');
    }

}
