/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting;

import org.opencms.flex.CmsFlexRequest;

/**
 * Callback interface to allow a resource loader to be called back if he should
 * update a resource from the request.
 * <p>
 * 
 */
public interface I_ScriptRequestUpdater {

    /**
     * Signals the implementor to update a resource from the request.
     * 
     * @param path
     *            The path
     * @param request
     *            The request
     */
    // TODO: Better Javadoc!
    public void updateScriptFromRequest(String path, CmsFlexRequest request);

}
