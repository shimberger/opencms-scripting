/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting;

import org.opencms.i18n.A_CmsMessageBundle;
import org.opencms.i18n.I_CmsMessageBundle;

/**
 * Convenience class to access the localized messages of this OpenCms package.
 * <p>
 */
public class Messages extends A_CmsMessageBundle {

    public static final String DEBUG_CMSEVENT_MODULE_ACTION_PURGING_1 = "DEBUG_CMSEVENT_MODULE_ACTION_PURGING_1";

    public static final String DEBUG_CMSEVENT_MODULE_ACTION_PURGE_RECEIVED_0 = "DEBUG_CMSEVENT_MODULE_ACTION_PURGE_RECEIVED_0";

    public static final String DEBUG_CMSEVENT_MODULE_ACTION_0 = "DEBUG_CMSEVENT_MODULE_ACTION_0";

    public static final String ADDING_CONFIGURATION_PARAM_2 = "ADDING_CONFIGURATION_PARAM_2";

    public static final String LOG_IGNORING_EXC_1 = "LOG_IGNORING_EXC_1";

    /** Name of the used resource bundle. */
    private static final String BUNDLE_NAME = "org.opencms.scripting.messages";

    /** Static instance member. */
    private static final I_CmsMessageBundle INSTANCE = new Messages();

    /**
     * Hides the public constructor for this utility class.
     * <p>
     */
    private Messages() {

        // hide the constructor
    }

    /**
     * Returns an instance of this localized message accessor.
     * <p>
     * 
     * @return an instance of this localized message accessor
     */
    public static I_CmsMessageBundle get() {

        return INSTANCE;
    }

    /**
     * Returns the bundle name for this OpenCms package.
     * <p>
     * 
     * @return the bundle name for this OpenCms package
     */
    public String getBundleName() {

        return BUNDLE_NAME;
    }

}
