/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.opencms.configuration.CmsConfigurationManager;
import org.opencms.file.CmsObject;
import org.opencms.loader.I_CmsResourceLoader;
import org.opencms.main.CmsEvent;
import org.opencms.main.CmsLog;
import org.opencms.main.I_CmsEventListener;
import org.opencms.main.OpenCms;
import org.opencms.module.A_CmsModuleAction;
import org.opencms.module.CmsModule;

/**
 * Module action to clear script repositories on event.
 * <p>
 * 
 * This class listens for events indicating a JSP repository purge and ensures
 * that all languages integrations that use a repository in the RFS get notified
 * to purge their repository too.
 */
public class CmsScriptingModuleAction extends A_CmsModuleAction {

    /**
     * The logging channel for this class.
     */
    private static Log LOG = CmsLog.getLog(CmsScriptingModuleAction.class);

    /**
     * @see A_CmsModuleAction#initialize(CmsObject, CmsConfigurationManager,
     *      CmsModule)
     */
    public void initialize(CmsObject adminCms,
            CmsConfigurationManager configurationManager, CmsModule module) {
        super.initialize(adminCms, configurationManager, module);
        // Register this object as an OpenCms event listener
        OpenCms.getEventManager().addCmsEventListener(this);
    }

    /**
     * Listens for Flex events denoting a JSP repository purge.
     * 
     * @see A_CmsModuleAction#cmsEvent(CmsEvent)
     */
    public void cmsEvent(CmsEvent event) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(Messages.get().getBundle().key(
                    Messages.DEBUG_CMSEVENT_MODULE_ACTION_0));
        }
        if (I_CmsEventListener.EVENT_FLEX_PURGE_JSP_REPOSITORY == event
                .getType()) {
            /*
             * Loop over all resource loaders registered in OpenCms and check if
             * they're a script resource loader backed by a RFS repository. If
             * so, call the resource loaders purgeRepository() method.
             */
            if (LOG.isDebugEnabled()) {
                LOG
                        .debug(Messages
                                .get()
                                .getBundle()
                                .key(
                                        Messages.DEBUG_CMSEVENT_MODULE_ACTION_PURGE_RECEIVED_0));
            }
            List resourceLoaders = OpenCms.getResourceManager().getLoaders();
            for (Iterator i = resourceLoaders.iterator(); i.hasNext();) {
                I_CmsResourceLoader loader = (I_CmsResourceLoader) i.next();
                if (loader instanceof I_CmsScriptRepositoryBacked) {
                    I_CmsScriptRepositoryBacked scriptLoader = (I_CmsScriptRepositoryBacked) loader;
                    scriptLoader.purgeRepository();
                    if (LOG.isInfoEnabled()) {
                        LOG
                                .info(Messages
                                        .get()
                                        .getBundle()
                                        .key(
                                                Messages.DEBUG_CMSEVENT_MODULE_ACTION_PURGING_1,
                                                loader.getResourceLoaderInfo()));
                    }
                }
            }
        }
    }

}
