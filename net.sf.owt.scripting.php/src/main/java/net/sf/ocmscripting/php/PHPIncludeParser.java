/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.php;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple RegEx based parser which tries to find all includes inside a PHP file
 * and calls back a visitor.
 * <p>
 * 
 */
public class PHPIncludeParser {

    /**
     * PHP include functions
     */
    private static final String DEFAULT_INCLUDE_FUNCTIONS_ARRAY[] = {
            "include", "include_once", "require", "require_once" };

    /**
     * PHP include functions as List
     */
    private static final List DEFAULT_INCLUDE_FUNCTIONS = Arrays
            .asList(DEFAULT_INCLUDE_FUNCTIONS_ARRAY);

    /**
     * The compiled pattern
     */
    private static Pattern PATTERN = null;

    /**
     * PHP start directive String
     */
    private String startDirective = "<?php";

    /**
     * PHP end directive String
     */
    private String endDirective = "?>";

    /**
     * Parses a String of PHP code and calls back the provided visitor.
     * 
     * @param code
     *            The code to parse
     * @param visitor
     *            The visitor to call back
     */
    public void parseString(String code, PHPIncludeVisitor visitor) {

        int currentPosition = 0;
        int nextStartDirectivePosition = code.indexOf(startDirective,
                currentPosition);

        if (nextStartDirectivePosition == -1) {
            // we will return the code if no php directive is found
            visitor.visitSurroundingText(code);
            return;
        }

        // while there is a next PHP start directive
        while (nextStartDirectivePosition != -1) {

            /*
             * Set the current start directive and look for the end directive
             */
            int currentStartDirective = nextStartDirectivePosition;
            int currentEndDirective = code.indexOf(endDirective,
                    currentStartDirective);

            // Determine start position and end position inside the string
            int startPosition = currentStartDirective + startDirective.length();
            int endPosition = currentEndDirective;

            // Extract the code before the PHP code and after the last PHP block
            visitor.visitSurroundingText(code.substring(currentPosition,
                    startPosition));

            // Parse the PHP code block
            parseCodeBlock(code.substring(startPosition, endPosition), visitor);

            // Extract the end directive
            visitor.visitSurroundingText(code.substring(currentEndDirective,
                    currentEndDirective + endDirective.length()));

            // try to find the next code block
            currentPosition = currentEndDirective + endDirective.length();
            nextStartDirectivePosition = code.indexOf(startDirective,
                    currentPosition);
        }

        // Visit the remaining file (after the last PHP block)
        visitor.visitSurroundingText(code.substring(currentPosition));

    }

    /**
     * Parses a PHP code block.
     * 
     * @param codeBlock
     *            The PHP code block without the directives
     * @param visitor
     *            The visitor to call back
     */
    private void parseCodeBlock(String codeBlock, PHPIncludeVisitor visitor) {
        Matcher matcher = getIncludeFunctionPattern().matcher(codeBlock);

        int currentPosition = 0;

        // while include functions are found
        while (matcher.find()) {

            /*
             * The prefixStart determines the start of the prefix text before
             * the actual include path. The end determines the end before the
             * include path string begins. Group 3 is the include path.
             */
            int prefixStart = currentPosition;
            int prefixEnd = matcher.start(3);

            /*
             * The suffix is the same as the prefix but after the include path
             * string.
             */
            int suffixStart = matcher.end(3);
            int suffixEnd = matcher.end();

            String prefix = codeBlock.substring(prefixStart, prefixEnd);
            String include = matcher.group(3);
            String suffix = codeBlock.substring(suffixStart, suffixEnd);

            // visit the prefix text, the path and then the suffix text
            visitor.visitSurroundingText(prefix);
            visitor.visitIncludeFile(include);
            visitor.visitSurroundingText(suffix);

            currentPosition = suffixEnd;
        }

        // if there are chars left.. simply append it as text
        if (currentPosition < codeBlock.length() - 1) {
            visitor.visitSurroundingText(codeBlock.substring(currentPosition));
        }

    }

    /**
     * Returns the compiled Pattern to find include functions.
     * <p>
     * 
     * @return The Pattern to find include functions.
     */
    private static Pattern getIncludeFunctionPattern() {
        // lazy initialization
        if (PATTERN != null) {
            return PATTERN;
        }

        /*
         * Build up the RegEx string.
         */
        StringBuffer regularExpression = new StringBuffer();
        regularExpression.append("(");
        // build a string like function1|functionb for matching
        for (Iterator i = DEFAULT_INCLUDE_FUNCTIONS.iterator(); i.hasNext();) {
            String includeFunctionName = (String) i.next();
            regularExpression.append(includeFunctionName);
            if (i.hasNext()) {
                regularExpression.append("|");
            }
        }
        regularExpression.append(")");
        // functionName + (zero or more whitespaces) + (0-1 () + (" or ') +
        regularExpression
                .append("\\s*\\(?(\"|\')(.*?)(\"|\')\\)?\\s*(;|\\z|or)");

        PATTERN = Pattern.compile(regularExpression.toString());
        return PATTERN;
    }

}
