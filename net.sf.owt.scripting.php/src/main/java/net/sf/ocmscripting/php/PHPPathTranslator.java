/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.php;

import net.sf.ocmscripting.CmsScriptingUtil;

import org.opencms.util.CmsFileUtil;

/**
 * Class which translates a path to be absolute inside the VFS.
 */
public class PHPPathTranslator {

    /**
     * The current working directory
     */
    private String currentDirectory;

    /**
     * Creates a new PHPPathTranslator.
     * 
     * @param currentDirectory
     *            The current working directory as a absolute path.
     */
    public PHPPathTranslator(String currentDirectory) {
        this.currentDirectory = currentDirectory;
    }

    /**
     * Translate the path to be absolute.
     * 
     * @param path
     *            The path to translate
     * @return The translated path
     */
    public String translate(String path) {
        String translatedPath = path;
        if (!CmsScriptingUtil.isAbsolutePath(path)) {
            // If the include path is not absolute, calculate the absolute path
            // by prepending the current directory and then resolving all
            // relative elements ("../")
            translatedPath = CmsFileUtil.normalizePath(currentDirectory + path);
        }
        //TODO: Dynamic translation
        //translatedPath = "($request->getAttribute('" + CmsPHPScriptLoader.ATTR_PATH_TRANSLATOR + "')->translatePath(" + translatedPath + "))";
        return translatedPath;
    }

}
