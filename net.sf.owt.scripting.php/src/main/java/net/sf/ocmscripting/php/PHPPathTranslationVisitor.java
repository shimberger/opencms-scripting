/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.php;

import net.sf.ocmscripting.CmsScriptingUtil;

import org.opencms.file.CmsObject;

/**
 * A Visitor which transforms the given source code to contain
 * normalized/absolute RFS paths.
 * 
 */
public class PHPPathTranslationVisitor implements PHPIncludeVisitor {

    /**
     * The translator applied to the paths.
     */
    private PHPPathTranslator pathTranslator;

    /**
     * The translated source code.
     */
    private StringBuffer translatedContents;

    /**
     * The path to the repository.
     */
    private String repositoryPath;

    /**
     * The CmsObject used for reading the code.
     */
    private CmsObject cms;

    /**
     * Constructs a new Visitor of this kind.
     * 
     * @param pathTranslator
     *            The translator to be used
     * @param repositoryPath
     *            The path to the repository
     * @param cms
     *            The CmsObject used to read the source
     */
    public PHPPathTranslationVisitor(PHPPathTranslator pathTranslator,
            String repositoryPath, CmsObject cms) {
        this.pathTranslator = pathTranslator;
        this.repositoryPath = repositoryPath;
        this.cms = cms;
        this.translatedContents = new StringBuffer();
    }

    /**
     * @see PHPIncludeVisitor#visitIncludeFile(String)
     */
    public void visitIncludeFile(String file) {
        // Here we determine the complete and absolute RFS path
        // and write this into the include path.
        String translatedPath = pathTranslator.translate(file);
        
        translatedPath = CmsScriptingUtil.computeRFSPath(repositoryPath, cms,
                translatedPath);
        translatedPath = CmsScriptingUtil.ensureSuffix(translatedPath,
                CmsPHPScriptLoader.PHP_SUFFIX);
        translatedContents.append(translatedPath);
    }

    /**
     * @see PHPIncludeVisitor#visitSurroundingText(String)
     */
    public void visitSurroundingText(String text) {
        translatedContents.append(text);
    }

    /**
     * Returns the translated source code.
     * 
     * @return The translated source code
     */
    public String getTranslatedContents() {
        return translatedContents.toString();
    }

}
