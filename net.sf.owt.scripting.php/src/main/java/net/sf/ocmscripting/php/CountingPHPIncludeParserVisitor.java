/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.php;

/**
 * Simple {@link PHPIncludeVisitor} which counts the occurences of surrounding
 * text and includes.
 * <p>
 * 
 */
public class CountingPHPIncludeParserVisitor implements PHPIncludeVisitor {

    /**
     * Variable to hold the include count.
     */
    private int includes = 0;

    /**
     * Variable to hold the text count.
     */
    private int text = 0;

    /**
     * @see PHPIncludeVisitor#visitIncludeFile(String)
     */
    public void visitIncludeFile(String file) {
        this.includes++;
    }

    /**
     * @see PHPIncludeVisitor#visitSurroundingText(String)
     */
    public void visitSurroundingText(String text) {
        this.text++;
    }

    /**
     * Returns the number of counted includes.
     * 
     * @return The number of counted includes
     */
    public int getIncludes() {
        return includes;
    }

    /**
     * Returns the number of counted texts.
     * 
     * @return The number of counted texts
     */
    public int getText() {
        return text;
    }

    /**
     * Resets the internal counters to 0.
     */
    public void resetCount() {
        includes = 0;
        text = 0;
    }

}
