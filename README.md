OPENCMS SCRIPTING LANGUAGE INTEGRATION
======================================

    URL: https://bitbucket.org/shimberger/opencms-scripting

DESCRIPTION
-----------

This project provides scripting language integrations for OpenCms 7.
It's goal is to allow the use of other languages than Java/JSP 
inside of OpenCms. The reasoning for the integration is different
in every environment. Be it either knowledge (not all developers are
familiar with Java), speed (dynamic languages often have faster
turnaround times / development cycles) or some other (individual) reason.	

The project is structured into (currently) three subprojects:

    - The scripting core: Provides generic classes for scripting support
    - The PHP scripting module: Adds PHP support to the core 
    - The Groovy scripting module: Adds Groovy support to the core

If you're interested in extending the source please join the
project!

TERMS USED IN THIS DOCUMENT
---------------------------

Writing instructions is never easy. For the sake of clearance some
terms/variables are defined in this section to help you understand
the following text.

${webapp}:  This means the path to the OpenCms Webapplication
            on your hard disk (e.g. inside Tomcats "webapps" directory).

${source}:  denotes the path on your hard disk where the
            source code is stored. So "${source}/groovy/REQUIRED_JARS.txt"
            refers to the "groovy" directory parallel to this file 
            ("README.md").

${opencms}: This means the path inside your OpenCms VFS. So it
            determines a resource inside your CMS which is accessible
            through the OpenCms Workplace.             

BUILDING
--------

To checkout the source use the following steps:

    git clone git@bitbucket.org:shimberger/opencms-scripting.git
    cd opencms-scripting
    ./gradlew ocmsModule

Note: On Windows you have to use gradlew.bat
  		
INSTALLATION
------------

To install the modules open the OpenCms Workplace, enter thes module 
administration and upload/install the module ZIP files inside the 
"${source}/<module>/build/lib" directory. Be sure to start with the core 
module because the other modules will depend on the core. 
The core must be installed regardless of the installed language 
support modules.

CONFIGURING PHP
---------------

Add the following Servlets to your "${webapp}/WEB-INF/web.xml".
	
    <servlet>
        <servlet-name>PHPServlet</servlet-name>
        <servlet-class>com.caucho.quercus.servlet.QuercusServlet</servlet-class>
        <init-param>
            <param-name>ini-file</param-name>
            <param-value>WEB-INF/php.ini</param-value>
        </init-param>
    </servlet>

    <servlet-mapping>
        <servlet-name>PHPServlet</servlet-name>
        <url-pattern>*.php</url-pattern>
    </servlet-mapping>
	
Register the resource loader in your "${webapp}/WEB-INF/config/opencms-vfs.xml"
below the "<resourceloaders>"-node:

    <loader class="net.sf.ocmscripting.php.CmsPHPScriptLoader" />

Restart your application/server, log into the workplace and add the following to the 
"editor_configuration.xml" in "${opencms}/system/workplace/editors/simple/" below 
the "<resourcetypes>" node:

     <type>
         <name>php</name>
         <ranking>0</ranking>
         <mapto>php</mapto>
     </type>
         
Switch to the Administration View, open the Workplace Tools and click on 
"Re-Initialize the Workplace". This will pick up the new editor configuration.     

CONFIGURING GROOVY
------------------

Add the following Servlets to your "${webapp}/WEB-INF/web.xml":
	
    <servlet>
           <servlet-name>GroovyServlet</servlet-name>
           <servlet-class>net.sf.ocmscripting.groovy.CmsGroovyServlet</servlet-class>
    </servlet>
	    
    <servlet-mapping>
           <servlet-name>GroovyServlet</servlet-name>
           <url-pattern>*.groovy</url-pattern>
    </servlet-mapping>

Register the resource loader by adding the following node to the 
"${webapp}/WEB-INF/config/opencms-vfs.xml" file right below the 
"<resourceloaders>"-node:

    <loader class="net.sf.ocmscripting.groovy.CmsGroovyScriptLoader" />    

Restart your application/server, log into the workplace and add the following to the 
"editor_configuration.xml" in "${opencms}/system/workplace/editors/simple/" below 
the "<resourcetypes>"-node:

    <type>
        <name>groovlet</name>
        <ranking>0</ranking>
        <mapto>groovlet</mapto>
    </type>

Switch to the Administration View, open the Workplace Tools and click on 
"Re-Initialize the Workplace". This will pick up the new editor configuration. 
    
USAGE
-----

Simply create a new file using the OpenCms explorer choosing
the respective language file type.

Enter the script code into the file and open it as normal.

You can include your scripts using the <cms:include>-tag.

### EXAMPLE PHP SCRIPT

    <h1>PHP Demo</h1>

    <ul>	
    <?php

        $numbers = array(1,2,3,4,5,6);
        foreach ($numbers as $num) {
            echo '<li>'.$num.'</li>';
        }
    	
    ?>
    </ul> 
	
### EXAMPLE GROOVLET
	
    print "<h1>Example Groovlet</h1>"
	
    li = { print "<li>" + it + "</li>" }
    numbers = [1,2,3,4,5,6]
	
    print "<ul>"	
    numbers.each(li)		
    print "</ul>"